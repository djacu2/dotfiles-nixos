# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running `nixos-help`).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Make ready for nix flakes
  nix = {
    package = pkgs.nixUnstable;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
    };

  # Use the latest linux kernel to support
  boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.kernelModules = [ "kvm-intel" ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Define networking parameters
  networking.hostName = "adalon"; # define the hostname
  networking.networkmanager.enable = true;
  networking.interfaces.wlp170s0.useDHCP = true;

  # Set the time zone.
  time.timeZone = "America/Los_Angeles";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will me mandatory in the future, so this generated config
  # replicates the default behavior.
  networking.useDHCP = false;

  # Define a user account. Don't forget to set a password with `passwd`.
  users.users.bakerdn = {
    isNormalUser = true;
    extraGroups = [
        "wheel"
        "networkmanager"
    ];
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    # cli tools
    wget
    fish
    # other
    firefox
  ];

  fonts.fonts = with pkgs; [
    fira-code
    source-code-pro
    font-awesome
  ];

  fonts.fontconfig = {
    defaultFonts = {
      monospace = [
        "Fira Code"
        "Source Code Pro"
      ];
    };
  };

  # Hardware support
  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
    };
  };

  # List services you wish to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # This value determines the NixOS relesae from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It's perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value, read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.05"; # Did you read the comment?
}
